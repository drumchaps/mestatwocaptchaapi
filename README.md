Implements all different sort of captcha solvers that 2captcha.com offers.

HOW TO USE:


E.G. RECAPTCHA 2.0.


```python

from mestatwocaptchaapi.twocaptcha import TwoCaptchaApi

api_key = ""
image_path = ""
instructions = ""
rows = ""
cols = ""

twocaptcha = TwoCaptchaApi()
twocaptcha.set_key( api_key )
twocaptcha.send_recaptcha2( instructions, rows, cols, imagepath ) 

```



