
import requests


from mestatwocaptchaapi.statics import (
    SOLVED_RESPONSES,
    UPLOAD_RESPONSES,
    ENDPOINTS
)


class TwoCaptchaApi():
    
    ENDPOINTS = ENDPOINTS
    UPLOAD_RESPONSES = UPLOAD_RESPONSES
    SOLVED_RESPONSES = SOLVED_RESPONSES

    def __init__( self, apikey=None):
        """ Sets initial value.
        """
        if apikey:
            self.set_key( apikey )
        pass


    def set_key( self, apikey ):
        """ Sets the api key.
        """
        self.apikey = apikey
        pass

    
    def get_response( self, id ):
        """Returns the request result for a SENT captcha ID.
        """
        params = {}
        params["key"] = self.apikey
        params["action"] = "get"
        params["id"] = id
        return requests.get(
            self.ENDPOINTS["RESPONSE"],
            params = params
        )
        pass


    def parse_solved_response( self, response ):
        """ Looks out for a status response for a SOLVED captcha
        """
        for key in self.SOLVED_RESPONSES.keys():
            if key in response:
                return self.SOLVED_RESPONSES[ key ]
        raise Exception("Response:%s not implemented." % (response,))
        pass


    def send_recaptcha2( self, instructions, rows, cols, imagepath ):
        """ Sends a recaptcha 2 image and instructions to be decoded.
        """
        data = {}
        data["recaptcha"] = 1
        data["key"] = self.apikey
        data["method"] = "post"
        data["recaptcharows"] = rows
        data["recaptchacols"] = cols
        data["textinstructions"] = instructions
        with open( imagepath, "rb") as f:
            return requests.post(
                self.ENDPOINTS["INPUT"],
                data = data,
                files = {"file": f},
            )
        pass

    def send_captcha_instructions( self, instructions, imagepath ):
        """ Sends an image 
        """
        data = {}
        data["key"] = self.apikey
        data["method"] = "post"
        data["textinstructions"] = instructions
        with open( imagepath, "rb") as f:
            return requests.post(
                self.ENDPOINTS["INPUT"],
                data = data,
                files = { "file": f }
            )
        pass

    def claim_captcha( self, captcha_id ):
        """ Claim the received captcha_id as wrong answered.
        """
        params = {}
        params["key"] = self.apikey
        params["action"] = "reportbad"
        params["id"] = captcha_id
        params["method"] = "get"
        return  requests.get(
            self.ENDPOINTS["RESPONSE"],
            params = params
        )
        pass


    def parse_sent_response( self, response ):
        """ Looks out for a status response for a SENT captcha
        """
        for key in self.UPLOAD_RESPONSES:
            if key in response:
                return self.UPLOAD_RESPONSES[ key ]
                pass
            pass
        raise Exception("Response:%s not implemented." % (response,))
        pass
	



