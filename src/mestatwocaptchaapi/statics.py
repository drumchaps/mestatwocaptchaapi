


ENDPOINTS = {
    "INPUT" : "http://2captcha.com/in.php",
    "RESPONSE" : "http://2captcha.com/res.php"
}


UPLOAD_RESPONSES = {
    "OK": {
        "value": 0, 
        "description" : "The CAPTHCA is put in the queue.",
        "key": "OK"
    },
    "ERROR_WRONG_USER_KEY" : {
        "value": 1, 
        "description" : "Wrong \"key\" parameter format, it should contain 32 symbols.",
        "key": "ERROR_WRONG_USER_KEY",
        "status": "denied"
    },
    "ERROR_KEY_DOES_NOT_EXIST": {
        "value": 2, 
        "description" : "The \"key\" doesn't exist ",
        "key": "ERROR_KEY_DOES_NOT_EXIST",
        "status": "denied"
    },
    "ERROR_ZERO_BALANCE": {
        "value": 2, 
        "description" : "You don't have money on your account",
        "key": "ERROR_ZERO_BALANCE",
        "status": "denied"
    },
    "ERROR_NO_SLOT_AVAILABLE": {
        "value": 3,
        "description": "This error can have two causes: 1) There is queue of captchas sent from your account which is exceedingly long and they haven't been assigned to the workers yet. Maximal number of captchas in such queue from one account can be between 50 and 100, depending on the total number of captchas in the queue awaiting ther assignment.\
2) If you are sending regular captchas ( as opposed to recaptchas ) this error can happen if yoy have set your maximum rate lower than the current rate on the server. P.S: This error became particularly substantial after we've added the new userrecaptch method, as it allows you to send hundreds of captchas in the space of one second. If you've got this error - make a one second pause and repeat your request.",
         "key": "ERROR_CAPTCHA_UNSOLVABLE",
         "status": "denied"
    },
    "ERROR_ZERO_CAPTCHA_FILESIZE": {
        "value": 4, 
            "description" : "CAPTCHA size is less than 100 bites",
            "key": "ERROR_ZERO_CAPTCHA_FILESIZE",
            "status": "denied",
    },
    "ERROR_TOO_BIG_CAPTCHA_FILESIZE": {
        "value": 5, 
        "description" : "CAPTCHA size is more than 100 Kbites",
        "key": "ERROR_TOO_BIG_CAPTCHA_FILESIZE",
        "status": "denied",
    },
    "ERROR_WRONG_FILE_EXTENSION": {
        "value": 6, 
        "description" : "The CAPTCHA has a wrong extension.Possible extensions are:jpg,jpeg,gif,png",
        "key": "ERROR_WRONG_FILE_EXTENSION",
        "status": "denied",
    },
    "ERROR_IMAGE_TYPE_NOT_SUPPORTED": {
        "value": 7, 
        "description" : "The server cannot recognize the CAPTCHA file type.",
        "key": "ERROR_IMAGE_TYPE_NOT_SUPPORTED",
        "status": "denied",
    },
    "ERROR_IP_NOT_ALLOWED": {
        "value": 8, 
        "description" : "The request has sent from the IP that is not on the list of your IPs. Check the list of your IPs in the system.",
        "key": "ERROR_IP_NOT_ALLOWED",
        "status": "denied",
    },
    "IP_BANNED": {
        "value": 9, 
        "description" : "The IP address you're trying to access our server with is banned due to many frequent attemprs to access the server using the wrong authorization keys. To lift the ban, please, contact our support team via email: support@2captcha.com",
        "key": "IP_BANNED",
        "status": "denied",
    },
    "ERROR_CAPTCHAIMAGE_BLOCKED": {
        "value": 10, 
        "description" : "You have sent an image, that is unrecognizable and which is saved in our database as such. Usually this happens when the site where you get the captcha from has stopped sending you captcha and started giving you a \"deny access\" cap.",
        "key": "ERROR_CAPTCHAIMAGE_BLOCKED",
        "status": "denied",
    }
}

SOLVED_RESPONSES = {
    "OK": {
        "value": 0, 
        "description" : "CAPTCHA solved successfully",
        "key": "OK",
        "status": "done"
    },
    "CAPCHA_NOT_READY" : {
        "value": 1, 
        "description" : "CAPTCHA is being solved,repeat the request several seconds later",
        "key": "CAPTCHA_NOT_READY",
        "status": "in progress"
    },
    "ERROR_KEY_DOES_NOT_EXIST": {
        "value": 2, 
        "description" : "You ussed the wrong key in the query",
        "key": "ERROR_KEY_DOES_NOT_EXIST",
        "status": "error"
    },
    "ERROR_WRONG_ID_FORMAT": {
        "value": 3, 
        "description" : "Wrong format ID CAPTCHA. ID must contain only numbers",
        "key": "ERROR_WRONG_ID_FORMAT",
        "status": "error"
    },
    "ERROR_CAPTCHA_UNSOLVABLE": {
        "value": 4,
        "description": "Captcha could not solve three different employee. Funds for this captcha not",
        "key": "ERROR_CAPTCHA_UNSOLVABLE",
        "status": "error",
    }
}

