
from distutils.core import setup
setup(
    name='mestatwocaptchaapi',
    version='0.0.1',
    author="Chaps",
    author_email="drumchaps@gmail.com",
    maintainer="Chaps",
    maintainer_email="drumchaps@gmail.com",
    url="https://bitbucket.org/drumchaps/mestatwocaptchaapi",                                                                                                                    
    packages  = [
        "mestatwocaptchaapi",
    ],
    package_dir={
        'mestatwocaptchaapi': 'src/mestatwocaptchaapi',
    },
    #install_requires = [ "requests", ]
)
                                                               
